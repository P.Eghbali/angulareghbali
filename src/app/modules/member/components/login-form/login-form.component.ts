import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  @Input() form: FormGroup;
  @Input() isPending: boolean;
  @Output() onSubmit = new EventEmitter<any>();
  public formValidation = {
    email: [
      {
        type: 'required',
        message: 'لطفا پست الکترونیکی را وارد نمایید',
      },
      {
        type: 'email',
        message: 'لطفا یک ایمیل معتبر وارد نمایید',
      },
    ],
    password: [
      {
        type: 'required',
        message: 'لطفا رمز عبور را وارد نمایید',
      },
      {
        type: 'minlength',
        message: 'رمز عبور باید بیشتر از 6 رقم باشد',
      },
    ],
  };
  constructor() {}

  ngOnInit(): void {}

  onLogin(event: any) {
    event.preventDefault();
    this.onSubmit.emit();
  }

  keyDownFunction(event: any) {
    if (event.keyCode == 13) {
      this.onLogin(event);
    }
  }
}
