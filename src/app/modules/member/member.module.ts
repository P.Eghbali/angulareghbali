import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login.component';
import { MemberRoutingModule } from './member-routing.module';

import { BrowserModule } from '@angular/platform-browser';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';

import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '@app/shared';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators'; // <-- #2 import module

import { CoreModule } from '@app/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginFormComponent } from './components';

@NgModule({
  declarations: [LoginComponent, LoginFormComponent],
  imports: [
    CoreModule,
    SharedModule,
    MemberRoutingModule,
    RouterModule,
    FormsModule,
    RxReactiveFormsModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpModule,
    MatTableModule,
    MatInputModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
  ],
})
export class MemberModule {}
