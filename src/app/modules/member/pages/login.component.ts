import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/core';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ProfileService } from '@app/core/profile.service';
import { NotifierService } from 'angular-notifier';
import { Helpers, matchValues } from '@app/shared/helpers';

import { CredentialsService } from '@app/core/authentication/credentials.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app.login.component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public isPending = false;
  public readonly notifier: NotifierService;
  public helpers = new Helpers();

  loginForm: FormGroup;

  constructor(
    public router: Router,
    private profileService: ProfileService,
    private httpClient: HttpClient,
    private _formBuilder: FormBuilder,
    private authService: AuthenticationService,

    // public readonly notifier: NotifierService,
    private credentialsService: CredentialsService
  ) {}

  ngOnInit() {
    this.loginForm = this._formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
    });
  }

  handleLogin() {
    if (this.loginForm.valid) {
      this.isPending = true;
      this.authService.login(this.loginForm.value).subscribe(
        (res) => {
          this.profileService.getProfile().subscribe((profile) => {
            this.isPending = false;
            this.router.navigate(['/dashboard/list']);
            this.notifier.notify('success', 'ورود شما با موفقیت انجام شد');
            return false;
          });
        },
        ({ error }) => {
          this.isPending = false;
          this.notifier.notify('error', error.message);
          return false;
        }
      );
    } else {
      this.helpers.validateFormFields(this.loginForm);
    }
  }
}
