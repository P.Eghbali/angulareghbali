import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './pages/dashboard.component';
import { AddHotelComponent } from './components/add-hotel/add-hotel.component';
import { AuthenticationGuard } from '@app/core/auth-guard.service';

const routes: Routes = [
  {
    path: 'dashboard',
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: 'list',
        component: DashboardComponent,
      },
      {
        path: 'add',
        component: AddHotelComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
