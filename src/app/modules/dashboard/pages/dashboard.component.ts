import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { NotifierService } from 'angular-notifier';
import { CredentialsService } from '@app/core/';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Helpers } from '@app/shared/helpers';
import { HotelService } from '@app/core/services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  hotelList: any[] = [];
  current_page: number = 1;
  formPending: boolean = false;
  message: string;
  hotelCount: number;
  isEmpty: boolean = false;
  tablePending: boolean = false;
  showHideAddContainer: boolean = false;
  isUpdate: boolean = false;
  public helpers = new Helpers();
  data: object = {
    id: '',
    hotel_name: '',
    count_room: '',
    phone_number: '',
    email: '',
  };
  hotelForm: FormGroup;
  columnsStructure: object[] = [
    {
      title: 'id',
      text: 'کد هتل',
      type: 'text',
    },
    {
      title: 'hotel_name',
      text: 'نام هتل',
      type: 'text',
    },
    {
      title: 'count_room',
      text: 'تعداد اتاق',
      type: 'text',
    },
    {
      title: 'phone_number',
      text: 'تلفن همراه',
      type: 'text',
    },
    {
      title: 'email',
      text: 'ایمیل',
      type: 'text',
    },
    {
      title: 'created_at',
      text: 'تاریخ ایجاد',
      type: 'text',
    },
    {
      title: 'operation',
      text: 'عملیات',
      type: 'action',
      actions: [
        {
          title: 'ویرایش',
          event: 'update',
          icon: 'edit',
          show: true,
        },
        {
          title: 'حذف',
          event: 'delete',
          icon: 'delete',
          show: true,
        },
      ],
    },
  ];
  public readonly notifier: NotifierService;

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private hotelService: HotelService,
    private _formBuilder: FormBuilder,
    private notifierSystem: NotifierService,
    private credentialsService: CredentialsService
  ) {
    this.notifier = notifierSystem;
  }

  ngOnInit(): void {
    this.hotelForm = this.buildForm();
    this.getHotels();
  }

  buildForm() {
    return this._formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      hotel_name: ['', Validators.required],
      count_room: ['', Validators.required],
      phone_number: ['', Validators.required],
    });
  }
  getHotels(havePending: boolean = true) {
    this.tablePending = havePending;
    this.hotelService.getHotels().subscribe(
      (list: any) => {
        if (list.length > 0) {
          this.hotelList = list;
          this.isEmpty = false;
        } else {
          this.isEmpty = true;
          this.message = 'رزروی ثبت نشده است.';
        }
        this.tablePending = false;
      },
      ({ error }) => {
        this.message =
          'لطفا اتصال خود به اینترنت را بررسی کنید و صفحه را بارگزاری کنید.';
        this.tablePending = false;

        this.notifier.notify('error', error.message);
      }
    );
  }

  onAddHotel() {
    var finalForm = this.hotelForm.value;
    if (this.hotelForm.valid) {
      this.formPending = true;
      this.hotelService.addHotel(finalForm).subscribe(
        (res) => {
          this.data = {};
          this.getHotels();
          this.formPending = false;
          this.showHideAddContainer = false;
          this.notifier.notify('success', 'هتل با موفقیت اضافه شد');
        },
        ({ error }) => {
          this.message =
            'لطفا اتصال خود به اینترنت را بررسی کنید و صفحه را بارگزاری کنید.';
        }
      );
    } else {
      this.helpers.validateFormFields(this.hotelForm);
    }
  }

  onUpdateHotel() {
    var finalForm = this.hotelForm.value;
    if (this.hotelForm.valid) {
      this.formPending = true;
      this.hotelService.updateHotel(this.data['id'], finalForm).subscribe(
        (res) => {
          this.data = {};
          this.getHotels();
          this.formPending = false;
          this.showHideAddContainer = false;
          this.notifier.notify('success', 'هتل با موفقیت ویرایش شد');
        },
        ({ error }) => {
          this.message =
            'لطفا اتصال خود به اینترنت را بررسی کنید و صفحه را بارگزاری کنید.';
        }
      );
    } else {
      this.helpers.validateFormFields(this.hotelForm);
    }
  }

  onDeleteHotel() {
    this.hotelService.deleteHotel(this.data['id']).subscribe(
      (res) => {
        this.data = {};
        this.getHotels();
        this.formPending = false;
        this.showHideAddContainer = false;
        this.notifier.notify('success', 'هتل با موفقیت حذف شد');
      },
      ({ error }) => {
        this.message =
          'لطفا اتصال خود به اینترنت را بررسی کنید و صفحه را بارگزاری کنید.';
      }
    );
  }

  onLogout(): Observable<boolean> {
    this.router.navigate(['/login']);
    this.credentialsService.setCredentials();
    return of(true);
  }
  handleHotel() {
    this.data = {};
    this.isUpdate = false;
    this.showHideAddContainer = !this.showHideAddContainer;
  }

  handleActions(event: any) {
    this.data['id'] = event.data.id;
    if (event.clickType == 'update') {
      this.data['hotel_name'] = event.data.hotel_name;
      this.data['count_room'] = event.data.count_room;
      this.data['phone_number'] = event.data.phone_number;
      this.data['email'] = event.data.email;
      this.showHideAddContainer = true;
      this.isUpdate = true;
    } else if (event.clickType == 'delete') {
      this.onDeleteHotel();
    }
  }
}
