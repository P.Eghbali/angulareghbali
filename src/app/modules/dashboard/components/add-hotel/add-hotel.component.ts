import {
  Component,
  ChangeDetectorRef,
  EventEmitter,
  Input,
  Output,
  OnInit,
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-add-hotel',
  templateUrl: './add-hotel.component.html',
  styleUrls: ['./add-hotel.component.scss'],
})
export class AddHotelComponent implements OnInit {
  @Input() mode: string;
  @Input() form: FormGroup;
  @Input() hotelData: object;
  @Input() isPending: boolean;
  @Input() isUpdate: boolean;
  @Output() onAdd = new EventEmitter<any>();
  @Output() onUpdate = new EventEmitter<any>();
  @Output() onClose = new EventEmitter<any>();
  public formValidation = {
    hotel_name: [
      {
        type: 'required',
        message: 'لطفا نام هتل را وارد نمایید',
      },
    ],
    count_room: [
      {
        type: 'required',
        message: 'لطفا تعداد اتاق وارد نمایید',
      },
    ],
    phone_number: [
      {
        type: 'required',
        message: 'لطفاتلفن همراه را وارد نمایید',
      },
      {
        type: 'minlength',
        message: 'تلفن همراه باید بیشتر از 6 رقم باشد',
      },
    ],
    email: [
      {
        type: 'required',
        message: 'لطفا پست الکترونیکی را وارد نمایید',
      },
      {
        type: 'email',
        message: 'لطفا یک ایمیل معتبر وارد نمایید',
      },
    ],
  };
  constructor(private cdref: ChangeDetectorRef) {}

  ngOnInit() {
    if (Object.keys(this.hotelData).length !== 0) {
      this.form.setValue({
        hotel_name: this.hotelData['hotel_name'],
        email: this.hotelData['email'],
        phone_number: this.hotelData['phone_number'],
        count_room: this.hotelData['count_room'],
      });
    }
  }
  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  keyDownFunction(event: any) {
    if (event.keyCode == 13) {
      this.onAddHotel();
    }
  }

  onAddHotel() {
    this.onAdd.emit();
  }
  onUpdateHotel() {
    this.onUpdate.emit();
  }
  onCloseDialog() {
    this.onClose.emit();
  }
}
