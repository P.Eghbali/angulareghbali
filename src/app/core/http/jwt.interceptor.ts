import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { HttpCacheService } from './http-cache.service';
import { CredentialsService } from '../authentication/credentials.service';

@Injectable({
  providedIn: 'root',
})
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private credentialsService: CredentialsService,
    private router: Router
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (
      this.credentialsService.isAuthenticated() &&
      this.router.url !== '/dashboard/list'
    ) {
      request = request.clone({
        setHeaders: {
          Accept: 'application/json',
          'Content-Encoding': 'gzip,deflate',
          Authorization: `Bearer ${this.credentialsService.credentials}`,
        },
      });
    } else {
      request = request.clone({
        setHeaders: {
          Accept: 'application/json',
        },
      });
    }
    return next.handle(request);
  }
}
