import { Injectable } from '@angular/core';
import { Observable, throwError, of, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {
  Credentials,
  CredentialsService,
} from './authentication/credentials.service';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  public userProfile = new BehaviorSubject<any>({});

  constructor(
    private credentialsService: CredentialsService,
    private httpClient: HttpClient
  ) {}

  getProfile() {
    if (this.credentialsService.isAuthenticated()) {
      return this.httpClient
        .get('https://api.farazhealth.ir/api/user/profile')
        .pipe(
          map((res: any) => {
            this.userProfile.next(res.data.user);
            return res.data;
          }),
          catchError((error) => throwError(error))
        );
    }
  }
}
