export interface MemberContext {
  mobile_number: string;
  password: string;
  token: string;
}