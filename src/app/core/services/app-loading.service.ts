import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class AppLoadingService {
  private localLoading: boolean;
  private localPlaceholder: string = "";

  constructor() {}

  onStartLoading(placeholder: string = null) {
    this.localLoading = true;
    if (placeholder) {
      this.localPlaceholder = placeholder;
    }
  }

  onEndLoading() {
    this.localLoading = false;
    this.localPlaceholder = "";
  }

  public get isLoading(): boolean {
    return this.localLoading;
  }

  public get placeholder(): string {
    return this.localPlaceholder;
  }
}
