import { map } from 'rxjs/operators';
import { throwError, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class HotelService {
  constructor(private httpClient: HttpClient) {}

  getHotels() {
    return this.httpClient
      .disableApiPrefix()
      .get(`https://5df4cc487305820014bcc47e.mockapi.io/hotel/hotel`)
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError((error) => throwError(catchError))
      );
  }
  addHotel(hotel) {
    return this.httpClient
      .post('/hotel', { ...hotel })
      .pipe(
        map((res: any) => {
          return res.data;
        }),
        catchError(error => throwError(error))
      );
  }

  updateHotel(hotel_id: number, hotel: any) {
    return this.httpClient
      .put(`/hotel/${hotel_id}`, { ...hotel })
      .pipe(
        map((res: any) => {
          return res.data;
        }),
        catchError(error => throwError(error))
      );
  }

  deleteHotel(hotel_id: number) {
    return this.httpClient
      .delete(`/hotel/${hotel_id}`)
      .pipe(
        map((res: any) => {
          return res.data;
        }),
        catchError(error => throwError(error))
      );
  }
}
