import { Injectable } from '@angular/core';
import { Observable, throwError, of, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { MemberContext } from '../models';
import { Credentials, CredentialsService } from './credentials.service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  mobileNumber = new BehaviorSubject<string>('');

  constructor(
    private credentialsService: CredentialsService,
    private httpClient: HttpClient
  ) {}

  login(loginContext: MemberContext): Observable<Credentials> {
    return this.httpClient.post('https://api.farazhealth.ir/api/auth/login', loginContext).pipe(
      map((res: any) => {
        this.credentialsService.setCredentials(res.data.token, true);
        return res.data;
      }),
      catchError((error: any) => {
        this.credentialsService.setCredentials();
        return throwError(error);
      })
    );
  }

  logout(): Observable<boolean> {
    this.credentialsService.setCredentials();
    return of(true);
  }
}
