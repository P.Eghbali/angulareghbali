import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from "./core/profile.service";
import { CredentialsService } from "./core/authentication/credentials.service";
import { AppLoadingService } from "./core/services/app-loading.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  isLoginPage: boolean = false;
  authPending: boolean = false;
  isAuthenticated: boolean;

  constructor(
    public router: Router,
    public appLoading: AppLoadingService,
    private profileService: ProfileService,
    private credentialService: CredentialsService
  ) {
    this.isAuthenticated = credentialService.isAuthenticated();
  }

  ngOnInit() {
    this.authPending = true;
    if (this.credentialService.isAuthenticated()) {
      this.profileService.getProfile().subscribe(
        () => {
          this.authPending = false;
        },
        () => {
          this.authPending = false;
          this.router.navigate(['/login']);
        }
      );
    } else {
      this.authPending = false;
      this.router.navigate(['/login']);
    }
  }
}
