import { Pipe, PipeTransform } from "@angular/core";
import * as numeral from "numeral";

@Pipe({
  name: "seprate"
})
export class NumberSepratePipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    if (value === 0 || value === "") {
      return value;
    } else {
      if (args[0] === "unformat") {
        return numeral(value).format("0");
      } else {
        return numeral(value).format("0,0");
      }
    }
  }
}
