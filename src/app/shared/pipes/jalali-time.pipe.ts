import { PipeTransform } from "@angular/core";
import { Pipe } from "@angular/core";

import * as moment from "moment-jalaali";

@Pipe({
  name: "jalali-time"
})
export class JalaliTimePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    // let MomentDate = moment(value, "LT YYYY/MM/DD");
    return moment(value).locale("fa").format("LT jYYYY/jMM/jDD ");
  }
}
