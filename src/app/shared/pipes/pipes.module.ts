import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NumberSepratePipe } from "./number-seprate.pipe";
import { SelectValuePipe } from "./select-value.pipe";
import { JalaliPipe } from './jalali.pipe';
import { JalaliTimePipe } from './jalali-time.pipe';
import { ShamsiDatetimePipe } from "./shamsi-datetime.pipe";
import { JalaliTimeLTRPipe } from './jalali-time-ltr.pipe';


@NgModule({
  declarations: [NumberSepratePipe, SelectValuePipe,JalaliPipe,JalaliTimePipe,ShamsiDatetimePipe,JalaliTimeLTRPipe],
  imports: [CommonModule],
  exports: [NumberSepratePipe, SelectValuePipe,JalaliPipe,JalaliTimePipe,ShamsiDatetimePipe,JalaliTimeLTRPipe],
  providers:[NumberSepratePipe, SelectValuePipe,JalaliPipe,JalaliTimePipe,JalaliTimeLTRPipe]
})
export class PipesModule {}
