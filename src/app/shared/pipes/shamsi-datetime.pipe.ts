import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment-jalaali";

@Pipe({
  name: "shamsiDatetime"
})
export class ShamsiDatetimePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    let MomentDate = moment(value);
    return MomentDate.locale("fa").format("LLLL");
  }
}
