import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "selectValue"
})
export class SelectValuePipe implements PipeTransform {
  transform(values: any[], ...args: any[]): any {
    return (
      values && args[0] && values.find(value => value.value === args[0]).label
    );
  }
}
