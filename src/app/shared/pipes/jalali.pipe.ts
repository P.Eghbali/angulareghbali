import { PipeTransform } from "@angular/core";
import { Pipe } from "@angular/core";

import * as moment from "moment-jalaali";

@Pipe({
  name: "jalali"
})
export class JalaliPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    let MomentDate = moment(value, "YYYY/MM/DD");
    return MomentDate.locale("fa").format("jYYYY/jMM/jDD");
  }
}
