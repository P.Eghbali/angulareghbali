import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";
import { PaginationInstance } from "ngx-pagination";

@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.scss"]
})
export class TableComponent implements OnInit {
  @Input() dataSource: any;
  @Input() columnsStructure: any;
  @Input() isPending: boolean = false;
  @Input() havePaginate: boolean = false;
  @Input() totalData:number;
  @Input() currentPage:number;

  @Output() pageChange = new EventEmitter<any>();
  @Output() onClickAction = new EventEmitter<any>();
  @Output() onCheckedRow = new EventEmitter<any>();
  @Output() onPayment = new EventEmitter<any>();

  selectedRow: any[] = [];
  displayedColumns: string[];
  public paginationConfig: PaginationInstance ;
  constructor() {}

  ngOnInit() {
    this.displayedColumns = this.columnsStructure.map(
      (column: any) => column.title
    );

    this.paginationConfig = {
      id: "hotelPagination",
      itemsPerPage: 15,
      currentPage: this.currentPage,
      totalItems:this.totalData
    }

  }

  onPageChange(number: any) {
    this.currentPage = number;
    this.pageChange.emit(number);
  }
  onPaymentBtn(element:object,type:string){
    this.onPayment.emit({data:element,clickType:type})
  }
  onAction(element: object, type: string) {
    this.onClickAction.emit({ data: element, clickType: type });
  }
  onChecked(element: any) {
    if (this.selectedRow.some(row => row.reserveId === element.reserveId)) {
      this.selectedRow.map((row, index) => {
        if (row.reserveId == element.reserveId) {
          this.selectedRow.splice(index, 1);
        }
      });
    } else {
      this.selectedRow.push(element);
    }
    this.onCheckedRow.emit(this.selectedRow);
  }
  showHideBtn(show:boolean,element:boolean){
    if (!show ) {
      if (!element) {
        return true;
      } else {
        return false
      }
    }else {
      return false;
    }
  }
}
