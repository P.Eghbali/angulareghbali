import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatRippleModule } from '@angular/material/core';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';

import { RatingModule } from 'ng-starrating';
import { AppFormFieldsModule } from './modules';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxPaginationModule } from 'ngx-pagination';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { TableComponent } from './components';

@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    RouterModule,
    MatInputModule,
    MatTableModule,
    RatingModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule,
    MatMenuModule,
    MatTooltipModule,
    AppFormFieldsModule,
    MatFormFieldModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatProgressBarModule,
    NgxDropzoneModule,
    NgxPaginationModule,
    NgxSkeletonLoaderModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    RatingModule,
    TableComponent,
    NgxDropzoneModule,
    MatFormFieldModule,
    NgxPaginationModule,
    AppFormFieldsModule,
    MatProgressSpinnerModule,
  ],
  declarations: [TableComponent],
})
export class SharedModule {}
