import * as numeral from "numeral";
import { FormGroup } from "@angular/forms";
import * as moment from "moment-jalaali";

export class Helpers {
  constructor() {}

  public NumberCounts = ["اول", "دوم", "سوم", "چهارم", "پنجم"];

  public validateFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field: any) => {
      const control = formGroup.get(field);
      if (formGroup.get(field)["controls"]) {
        Object.keys(formGroup.get(field)["controls"]).forEach((f: any) => {
          const ct = formGroup.get(field).get(f);
          ct.markAllAsTouched();
        });
      } else {
        control.markAsTouched();
      }
    });
  }

  public checkPriceNumber(
    number: any,
    title: string = "",
    minimum: any = -1,
    maximum: any = -1
  ) {
    if (
      String(number).match(/^[0-9]*$/) === null ||
      String(number).match(/^[0-9]*$/).input === " "
    ) {
      return `${title} را عدد صحیح وارد کنید یا زبان صغحه کلید خود را به انگلیسی تغییر دهید`;
    }

    if (Number(number) < 0) {
      return `${title} نمیتواند کمتر از صفر باشد`;
    }

    if (Number(number) < minimum && minimum < 0) {
      return `${title} نباید کمتر از ${minimum} باشد`;
    }

    if (Number(number) > maximum && maximum < 0) {
      return `${title} نباید بیشتر از ${maximum} باشد`;
    }

    return "";
  }

  public checkHotelSearchInfo(value: any) {
    const { checkIn, checkOut, cityId } = value;
    var isValid = true;
    if (!checkIn || !checkOut || !cityId) {
      isValid = false;
    } else if (
      !moment(checkIn, "YYYY-MM-DD", true).isValid() ||
      !moment(checkOut, "YYYY-MM-DD", true).isValid()
    ) {
      isValid = false;
    } else if (moment(checkOut).isBefore(checkIn)) {
      isValid = false;
    }

    return isValid;
  }

  public getUnformattedNumber(number: number) {
    return numeral(number).format("0");
  }

  public getFormattedDate(date: moment.Moment): string {
    return moment(date).format("YYYY-MM-DD");
  }
}
