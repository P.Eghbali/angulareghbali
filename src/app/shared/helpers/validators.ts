import { AbstractControl, ValidationErrors } from "@angular/forms";

export function nationalCode(control: AbstractControl) {
  const input = control.value;
  if (input.length === 0) return null;
  if (!/^\d{10}$/.test(input)) return { nationalCode: true };


  var check = parseInt(input[9]);
  var sum = 0;
  var i;
  for (i = 0; i < 9; ++i) {
    sum += parseInt(input[i]) * (10 - i);
  }
  sum %= 11;

  if ((sum < 2 && check == sum) || (sum >= 2 && check + sum == 11)) {
    return null;
  } else return { nationalCode: true };
}

export function matchValues(matchTo: string) {
  return (control: AbstractControl) => {
    return !!control.parent &&
      !!control.parent.value &&
      control.value === control.parent.controls[matchTo].value
      ? null
      : { ismatching: true };
  };
}
