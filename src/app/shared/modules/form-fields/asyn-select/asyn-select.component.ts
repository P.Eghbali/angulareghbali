import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "form-asyn-select",
  templateUrl: "./asyn-select.component.html",
  styleUrls: ["./asyn-select.component.scss"],
})
export class AsynSelectComponent {
  @Input() label: string;
  @Input() loading: boolean;
  @Input() bindLabel: string;
  @Input() placeholder: string;
  @Input() inputName: string;
  @Input() inputForm: FormGroup;
  @Input() validation: any[];
  @Input() mode: string;
  @Input() filter: boolean;
  @Input() isLoading: boolean;
  @Input() items: any;
  @Input() itemInput: any;
  @Input() required: boolean;
  @Input() title: string = null;
  @Input() showTitle: boolean = false;

  constructor() {}

  trackByFn(item: any) {
    return item.value;
  }
  getErrors() {
    return this.inputForm.controls[this.inputName]["errorMessage"];
  }
  onChange(event: any) {
    if (event) {
      this.title = event.label;
    } else {
      this.title = "";
    }
  }
}
