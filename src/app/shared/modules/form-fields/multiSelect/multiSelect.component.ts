import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "form-multiSelect",
  templateUrl: "./multiSelect.component.html",
  styleUrls: ["./multiSelect.component.scss"]
})
export class MutliSelectComponent implements OnInit {
  @Input() label: string;
  @Input() items: any[];
  @Input() loading: boolean;
  @Input() bindLabel: string;
  @Input() placeholder: string;
  @Input() inputName: string;
  @Input() inputForm: FormGroup;
  @Input() validation: any[];
  @Input() mode: string;
  @Input() isStar: boolean;
  @Input() required:boolean;
  @Input() multiple : boolean

  @Output() onSelect = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  public hasError(controlName: string, errorName: string) {
    return (
      this.inputForm.get(controlName).touched &&
      this.inputForm.get(controlName).hasError(errorName)
    );
  }

  onChange(event: any) {
    this.onSelect.emit(event);
  }

}
