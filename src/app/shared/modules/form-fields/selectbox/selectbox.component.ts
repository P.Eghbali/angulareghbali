import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from "@angular/core";
import { Observable } from "rxjs";
import { FormGroup } from "@angular/forms";

import { NgSelectConfig, NgSelectComponent } from "@ng-select/ng-select";
import { RxFormArray, RxFormGroup } from '@rxweb/reactive-form-validators';

@Component({
  selector: "form-selectbox",
  templateUrl: "./selectbox.component.html",
  styleUrls: ["./selectbox.component.scss"]
})
export class SelectboxComponent implements OnInit {
  @Input() label: string;
  @Input() items: any[];
  @Input() loading: boolean;
  @Input() required:boolean;
  @Input() bindLabel: string;
  @Input() placeholder: string;
  @Input() inputName: string;
  @Input() inputForm: any;
  @Input() validation: any[];
  @Input() mode: string;
  @Input() isStar: boolean;
  @Input() multiple : boolean;
  @Input() filter:boolean;

  @Output() onSelect = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  onChange(event: any) {
    this.onSelect.emit(event);
  }
  getErrors() {
    return this.inputForm.controls[this.inputName]["errorMessage"];
  }
}
