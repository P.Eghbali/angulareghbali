import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'form-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit {
  @Input() type: string;
  @Input() minimumNumber: string;
  @Input() maximumNumber: string;
  @Input() hint: string;
  @Input() label: string;
  @Input() suffix: string;
  @Input() isNumber: boolean;
  @Input() isPrice: boolean;
  @Input() isTiny: boolean;
  @Input() placeholder: string;
  @Input() inputName: string;
  @Input() inputForm: FormGroup;
  @Input() validation: any[];
  @Input() mode: string;
  @Input() noIcon: boolean;
  @Output() onBlur = new EventEmitter<any>();

  @ViewChild('autocomplete', { static: false }) autoComplete: ElementRef;

  constructor() {}

  onModelChange(event: any) {
    this.onBlur.emit(event);
  }

  ngOnInit() {}

  public hasError(controlName: string, errorName: string) {
    return this.inputForm.controls[controlName].hasError(errorName);
  }

  onClearValue() {
    this.inputForm.get(this.inputName).setValue('');
  }
  checkLabelAvailability() {
    return !this.isTiny && this.label;
  }
}
