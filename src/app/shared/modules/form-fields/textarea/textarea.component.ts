import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from '@angular/forms';

@Component({
  selector: "form-textarea",
  templateUrl: "./textarea.component.html",
  styleUrls: ["./textarea.component.scss"]
})
export class TextareaComponent implements OnInit {
  @Input() hint: string;
  @Input() label: string;
  @Input() placeholder: string;
  @Input() inputName: string;
  @Input() required:boolean;
  @Input() inputForm: FormGroup;
  @Input() validation: any[];
  @Input() mode: string;

  constructor() {}

  ngOnInit() {}

  public hasError(controlName: string, errorName: string) {
    return this.inputForm.controls[controlName].hasError(errorName);
  }
  getErrors() {
    return this.inputForm.controls[this.inputName]["errorMessage"];
  }
}
