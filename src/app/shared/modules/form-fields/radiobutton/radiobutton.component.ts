import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "form-radiobutton",
  templateUrl: "./radiobutton.component.html",
  styleUrls: ["./radiobutton.component.scss"]
})
export class RadiobuttonComponent implements OnInit {
  @Input() form: FormGroup;
  @Input() controlName: string;
  @Input() isChecked: boolean;

  @Input() items: any[]

  constructor() {}

  ngOnInit() {

  }
}
