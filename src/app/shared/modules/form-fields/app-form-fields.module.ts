import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { PipesModule } from '@app/shared/pipes/pipes.module';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


import { RatingModule } from 'ng-starrating';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

import { SelectboxComponent } from './selectbox/selectbox.component';
import { RadiobuttonComponent } from './radiobutton/radiobutton.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { InputComponent } from './input/input.component';
import { TextareaComponent } from './textarea/textarea.component';

import { ButtonComponent } from './button/button.component';
import { AsynSelectComponent } from './asyn-select/asyn-select.component';
import { MutliSelectComponent } from './multiSelect/multiSelect.component';
import {MatFormFieldModule} from '@angular/material/form-field';

@NgModule({
  declarations: [
    SelectboxComponent,
    RadiobuttonComponent,
    CheckboxComponent,
    InputComponent,
    TextareaComponent,
    ButtonComponent,
    AsynSelectComponent,
    MutliSelectComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    NgSelectModule,
    MatInputModule,
    MatRadioModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatSelectModule,
    RatingModule,
    MatProgressSpinnerModule,
    NgxMaterialTimepickerModule.setLocale('fa-IR'),
    FlexLayoutModule,
    PipesModule,
  ],
  exports: [
    SelectboxComponent,
    RadiobuttonComponent,
    CheckboxComponent,
    InputComponent,
    TextareaComponent,
    ButtonComponent,
    AsynSelectComponent,
    MutliSelectComponent,
  ],
})
export class AppFormFieldsModule {}
