import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "form-button",
  templateUrl: "./button.component.html",
  styleUrls: ["./button.component.scss"]
})
export class ButtonComponent {
  @Input() icon: string;
  @Input() failed: boolean;
  @Input() success: boolean;
  @Input() isLoading: boolean;
  @Input() color: string = "secondary";
  @Input() disabled:boolean;
  @Output() onClick = new EventEmitter<any>();

  constructor() {}

  handleClick(event: any) {
    this.onClick.emit(event);
  }
}
