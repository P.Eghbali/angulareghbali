import { Component, OnInit, Input, ChangeDetectorRef } from "@angular/core";

@Component({
  selector: "form-checkbox",
  templateUrl: "./checkbox.component.html",
  styleUrls: ["./checkbox.component.scss"]
})
export class CheckboxComponent implements OnInit {
  @Input() label: string;
  @Input() value: any;
  @Input() inputForm: any;
  @Input() formName: string;
  @Input() isChecked: boolean;

  constructor(private cdref: ChangeDetectorRef) {
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  ngOnInit() {
    this.isChecked = this.inputForm.includes(this.value);
  }

  onCheckChange(event: any) {
    const formArray = this.inputForm;
    if (event.checked) {
      formArray.push(event.source.value);
    } else {
      let i: number = 0;

      formArray.forEach((ctrl:any,index:number) => {
        if (ctrl == event.source.value) {
          formArray.splice(index,1);
          return;
        }
        i++;
      });
    }
  }
}
